﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerSetup : NetworkBehaviour {

	void Start ()
    {
        gameObject.name = "Player " + GetComponent<NetworkIdentity>().netId.ToString();
        EnablePlayer(true);
	}
    public void EnablePlayer(bool value)
    {
        if (!value || isLocalPlayer)
        {
            GetComponent<CharacterController>().enabled = value;
            GetComponent<FirstPersonController>().enabled = value;
            GetComponentInChildren<AudioListener>().enabled = value;
            GetComponentInChildren<Camera>().enabled = value;
        }

#warning fix is: value => !value
        if (!value)
        {
            GetComponent<PlayerController>().Health = 100;
            int index;
            switch (GetComponent<PlayerController>().Team)
            {
                case Team.These:
                    index = Random.Range(0, GameManager.Instance.theseRespawns.Count);
                    transform.position = GameManager.Instance.theseRespawns[index].position;
                    break;
                case Team.Those:
                    index = Random.Range(0, GameManager.Instance.thoseRespawns.Count);
                    transform.position = GameManager.Instance.thoseRespawns[index].position;
                    break;
            }
        }
#warning fix is: value => !value
        GetComponent<PlayerController>().isDead = !value;
    }
}
