﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public float RespawnTime;
    public List<Transform> theseRespawns;
    public List<Transform> thoseRespawns;

    public GameObject bulletPrefab;

    public static GameManager Instance = null;

	void Awake ()
    {
        if (Instance == null) Instance = this;
	}


}
