﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BulletSync : NetworkBehaviour {

    [SyncVar] Vector3 syncPos;
    public int speed;
    [SyncVar] public float lifeTimer;

    void Start ()
    {
        syncPos = transform.position;
	}
	
	void FixedUpdate ()
    {
	    if (isServer)
        {
            // move
            transform.Translate(
                Vector3.forward * speed * Time.deltaTime
            );
            syncPos = transform.position;
        }	
        if (isClient)
        {
            // smooth me!
            transform.position = Vector3.Lerp(
                transform.position,
                syncPos,
                5 * Time.deltaTime
            );
        }

        lifeTimer -= Time.fixedDeltaTime;

        if (lifeTimer <= 0)
        {
            NetworkServer.Destroy(gameObject);
        }
	}
}
