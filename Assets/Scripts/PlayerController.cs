﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public enum Team
{
    These,
    Those
}

public class PlayerController : NetworkBehaviour {
    [SyncVar] public int Health = 100;
    [SyncVar] public Team Team;
    public bool isDead = false;

    float respawnTimer;
    Camera cam;
    PlayerSync ps;

	void Start ()
    {
        Team = Team.These;
        cam = GetComponentInChildren<Camera>();
        ps = GetComponent<PlayerSync>();
    }

	void Update ()
    {
        if (isDead)
        {
            respawnTimer += Time.deltaTime;
            if (respawnTimer > GameManager.Instance.RespawnTime)
            {
                respawnTimer = 0;
                GetComponent<PlayerSetup>().EnablePlayer(true);
            }
        }
        if (!isLocalPlayer) return;

        RaycastHit hitInfo;
        if (Physics.Raycast(
            transform.position,
            Vector3.down,
            out hitInfo, 1.3f))
        {
            ps.grounded = 1;
        }
        else
        {
            ps.grounded = 0;
        }

        if (ps.grounded == 0)
        {
            ps.fSpeed = 0;
            ps.sSpeed = 0;
        }
        else
        {
            ps.fSpeed = Input.GetAxis("Vertical");
            ps.sSpeed = Input.GetAxis("Horizontal");
        }

        if (Input.GetMouseButtonDown(0))
        {
            CmdInstBullet();
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit = new RaycastHit();

            if (Physics.Raycast(ray, out hit, 500) && hit.transform.gameObject.GetComponent<PlayerController>())
            {
                CmdShoot(hit.transform.gameObject, 25);
            }
        }

        //aiming:
        if (Input.GetMouseButtonDown(1)) ps.isAiming = true;
        if (Input.GetMouseButtonUp(1)) ps.isAiming = false;


        /////////////////////

        //if (Input.GetKeyDown(KeyCode.Space)) animator.SetTrigger("Jump");        
        //if (Input.GetMouseButtonDown(0)) animator.SetTrigger("Shoot");
       
        
    }
    [ClientRpc]
    public void RpcGetDamage (int value)
    {
        Health -= value;
        if (Health <= 0)
        {
            GetComponent<PlayerSetup>().EnablePlayer(false);
        }
    }
    [Command]
    void CmdShoot(GameObject target, int damage)
    {
        target.GetComponent<PlayerController>().RpcGetDamage(damage);
    }
    [Command]
    void CmdInstBullet()
    {
        GameObject bullet = Instantiate<GameObject>(
            GameManager.Instance.bulletPrefab,
            GetComponentInChildren<Camera>().transform.position +
            GetComponentInChildren<Camera>().transform.forward,
            GetComponentInChildren<Camera>().transform.rotation
        );
        NetworkServer.Spawn(bullet);
    }
}
