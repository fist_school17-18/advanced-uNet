﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerSync : NetworkBehaviour {

    [SyncVar] Vector3 syncPos;
    [SyncVar] Quaternion syncRot;

    // animator params:
    [HideInInspector][SyncVar] public float grounded = 0;
    [HideInInspector][SyncVar] public float fSpeed;
    [HideInInspector][SyncVar] public float sSpeed;
    [HideInInspector][SyncVar] public bool isAiming;

    [Range(1, 30)][SerializeField] int lerpSpeed = 1;
    Animator animator;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }
    void FixedUpdate ()
    {
        LerpTransform();
        TransmitTransform();
	}

    void LerpTransform ()
    {
        if (!isLocalPlayer)
        {
            transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * lerpSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, syncRot, Time.deltaTime * lerpSpeed);

            float fsLerp = Mathf.Lerp(
                animator.GetFloat("ForwardSpeed"),
                fSpeed,
                Time.deltaTime * lerpSpeed
            );
            animator.SetFloat("ForwardSpeed", fsLerp);

            float ssLerp = Mathf.Lerp(
                animator.GetFloat("StrafeSpeed"),
                sSpeed,
                Time.deltaTime * lerpSpeed
            );
            animator.SetFloat("StrafeSpeed", ssLerp);

            float gLerp = Mathf.Lerp(
                animator.GetFloat("grounded"),
                grounded,
                Time.deltaTime * lerpSpeed
            );
            animator.SetFloat("grounded", gLerp);
        }
        else
        {
            animator.SetFloat("ForwardSpeed", fSpeed);
            animator.SetFloat("StrafeSpeed", sSpeed);
            animator.SetFloat("grounded", grounded);
        }
        animator.SetBool("isAiming", isAiming);
    }
    [Client]
    void TransmitTransform()
    {
        if (isLocalPlayer)
        {
            CmdSyncPos(transform.position);
            CmdSyncRot(transform.rotation);
            CmdSyncAnim(
                animator.GetFloat("ForwardSpeed"),
                animator.GetFloat("StrafeSpeed"),
                animator.GetFloat("grounded"),
                animator.GetBool("isAiming")
            );
        }
    }

    [Command]
    void CmdSyncPos(Vector3 pos)
    {
        syncPos = pos;
    }
    [Command]
    void CmdSyncRot(Quaternion rot)
    {
        syncRot = rot;
    }

    [Command]
    void CmdSyncAnim(float v, float h, float g, bool a)
    {
        fSpeed = v;
        sSpeed = h;
        grounded = g;
        isAiming = a;
    }
}
